require 'api_constraints'

Rails.application.routes.draw do
  mount SabisuRails::Engine => "/sabisu_rails"
  root 'home#index'

	# Api definition
  namespace :api, defaults: { format: :json } do
    scope module: :v1, constraints:	ApiConstraints.new(version:	1,	default: true)	do
    	devise_for :users
			resources :users, :only => [:show, :create, :update, :destroy]
			resources :admin_level_configs, 'admin-levels', 
        as: :admin_level_configs, controller: :admin_level_configs,  
        :only => [:show, :index, :create, :update, :destroy]
		end
  end

  match "*path", to: "home#index", via: :all
end
