require 'spec_helper'

describe AdminUnitConfig do
	let (:admin_unit){ FactoryGirl.build :admin_unit_config }
	subject {admin_unit}

	it {should respond_to (:name)}

	it { should belong_to :admin_level_config }
end