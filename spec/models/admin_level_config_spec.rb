require 'spec_helper'

describe AdminLevelConfig do
  let(:admin_level) { FactoryGirl.build :admin_level_config }
  subject { admin_level }

  it { should respond_to(:name) }

  it { should have_many(:admin_unit_configs) }
end