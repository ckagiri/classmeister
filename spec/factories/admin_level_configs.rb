FactoryGirl.define do 
	factory :admin_level_config do 
		name { "#{FFaker::Lorem.word.to_s}_#{rand(1000).to_s}" }
		description { FFaker::Lorem.sentence }
		parent_level_id 1
		status 'active'
	end
end