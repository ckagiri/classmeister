FactoryGirl.define do 
	factory :admin_unit_config do 
		name { FFaker::Lorem.word }
		code { FFaker::Lorem.word }
		description { FFaker::Lorem.sentence }
		admin_level_config
		parent_unit_id 1
		status 'active'
	end
end