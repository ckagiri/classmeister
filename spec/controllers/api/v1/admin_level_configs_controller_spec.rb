require 'spec_helper'

describe Api::V1::AdminLevelConfigsController do
	describe "GET #show" do
    before(:each) do
      @entity = FactoryGirl.create :admin_level_config
      get :show, id: @entity.id
    end

    it "returns the information about a reporter on a hash" do
      model_response = json_response[:admin_level_config]
      expect(model_response[:name]).to eql @entity.name
    end

    it { should respond_with 200 }
  end

  describe "GET #index" do
    before(:each) do
      4.times { FactoryGirl.create :admin_level_config }
      get :index
    end

    it "returns 4 records from the database" do
      model_response = json_response
      expect(model_response[:admin_level_configs]).to have(4).items
    end

    it { should respond_with 200 }
  end

  describe "POST #create" do
    before(:each) do
      @entity = FactoryGirl.attributes_for :admin_level_config
      post :create, { admin_level_config: @entity }
    end

    it "renders the json representation for the admin_level record just created" do
      model_response = json_response[:admin_level_config]
      expect(model_response[:name]).to eql @entity[:name]
    end

    it { should respond_with 201 }
  end

  describe "PUT/PATCH #update" do
    before(:each) do
      @entity = FactoryGirl.create :admin_level_config
      patch :update, { id: @entity.id, admin_level_config: { name: "whatever" } }
    end

    it "renders the json representation for the updated admin_level" do
      model_response = json_response[:admin_level_config]
      expect(model_response[:name]).to eql "whatever"
    end

    it { should respond_with 200 }
  end

  describe "DELETE #destroy" do
    before(:each) do
      @entity = FactoryGirl.create :admin_level_config
      delete :destroy, { id: @entity.id }
    end

    it { should respond_with 204 }
  end
end
