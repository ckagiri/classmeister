class Api::V1::AdminLevelConfigsController < ApplicationController
	respond_to :json

	def index
    list = AdminLevelConfig.all  
    render json: list, root: 'admin_levels' 
  end

	def show
		respond_with AdminLevelConfig.find(params[:id])
	end

	def create
    admin_level = AdminLevelConfig.new(model_params)
    if admin_level.save
      render json: admin_level, status: 201, location: [:api, admin_level]
    else
      render json: { errors: admin_level.errors }, status: 422
    end
  end

  def update
    admin_level = AdminLevelConfig.find(params[:id])

    if admin_level.update(model_params)
      render json: admin_level, status: 200, location: [:api, admin_level]
    else
      render json: { errors: admin_level.errors }, status: 422
    end
  end

  def destroy
    admin_level = AdminLevelConfig.find(params[:id])
    admin_level.destroy
    head 204
  end

  private

    def model_params
      params.require(:admin_level).permit(:name, :description, :parent_level_id, :status)
    end
end
