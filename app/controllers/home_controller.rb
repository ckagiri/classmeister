class HomeController < ApplicationController
  def index
    respond_to do |format|
      format.html
      # other formats you already support
      format.all { render text: '' }
    end
  end
end