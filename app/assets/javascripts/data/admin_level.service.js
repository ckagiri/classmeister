(function (){
	'use strict';

	angular
		.module('app')
		.factory('AdminLevel', Resource);

	Resource.$inject = ['railsResourceFactory']

	function Resource(railsResourceFactory){
		var resource = railsResourceFactory({
				url: '/api/admin-levels',
				name: 'adminLevel'
			});

		return resource;
	}
})();