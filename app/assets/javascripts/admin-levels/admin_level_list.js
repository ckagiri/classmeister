(function (){
	'use strict';

	angular
		.module('app')
		.controller('AdminLevelList', AdminLevelList);

	AdminLevelList.$inject = ['AdminLevel']

	function AdminLevelList(AdminLevel) {
		var vm = this;
		vm.adminLevels = [];
		vm.destroy = destroy

		load();

		function load(){
			return AdminLevel.query().then(function(data){
				vm.adminLevels = data;
				return vm.adminLevels;
			});
		}

		function destroy(adminLevel){
			alert('are you sure?')
			adminLevel.remove().then(function	(){
				load();
			}, function(error){
			});
		}
	}
})();