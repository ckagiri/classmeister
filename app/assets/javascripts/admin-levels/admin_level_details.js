(function (){
	'use strict';

	angular
		.module('app')
		.controller('AdminLevelDetails', AdminLevelDetails);

	AdminLevelDetails.$inject = ['$stateParams', 'AdminLevel']

	function AdminLevelDetails($stateParams, AdminLevel) {
		var id = null;
		var vm = this;
		vm.save = save;

		activate();

		function activate(){
			id = $stateParams.id;
			if(id){
				AdminLevel.get(id).then(function(data){
					vm.adminLevel = data
				})
			} else {
				vm.adminLevel = new AdminLevel()
			}						
		}

		function save(){
			if(id){
				update();
			} else {
				create();
			}
		}

		function create(){
			vm.adminLevel.create().then(function(data){
				}, function (error){
				});
		}

		function update(){
			vm.adminLevel.update().then(function(data){
				}, function (error){
				});
		}
	}
})();