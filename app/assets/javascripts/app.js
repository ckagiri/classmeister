(function	(){
	'use strict';

	var app = angular.module('app', [
			'ngAnimate',
			'ui.router',
			'templates',
			'rails'
		])

	app.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', '$httpProvider',
		function ($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider) {
			$stateProvider
				.state('app', {
					abstract: true,
					url: '/',
					views: {
						'content-container': {
							templateUrl: 'layout/content-two-column.html'
						}
					}
				})
				.state('app.home', {
					url: '',
					templateUrl: 'admin-levels/admin-level-list.html',
					controller: 'AdminLevelList',
					controllerAs: 'vm'
				})
				.state('app.admin-levels', {
					url: 'admin-levels',
					templateUrl: 'admin-levels/admin-level-list.html',
					controller: 'AdminLevelList',
					controllerAs: 'vm'
				})
				.state('app.admin-level-create', {
					url: 'admin-levels/create',
					templateUrl: 'admin-levels/admin-level-details.html',
					controller: 'AdminLevelDetails',
					controllerAs: 'vm'
				})
				.state('app.admin-level-details', {
					url: 'admin-levels/:id',
					templateUrl: 'admin-levels/admin-level-details.html',
					controller: 'AdminLevelDetails',
					controllerAs: 'vm'
				});
				
			$urlRouterProvider.otherwise('/');

			$locationProvider.html5Mode({
	       enabled:true,
	       requireBase: false
	     });
				
			//$httpProvider.defaults.headers.common['X-CSRF-Token'] = 
			//	document.getElementsByName("csrf-token")[0].content

			// By default, angular sends "application/json, text/plain, */*" 
			// which rails sees and focuses on the */* and sends html :-(
			$httpProvider.defaults.headers.common['Accept'] = "application/json"
		}]);
})();