class AdminUnitConfig < ActiveRecord::Base
	belongs_to :admin_level, class_name: 'AdminLevelConfig', foreign_key: :admin_level_id
	belongs_to :parent, class_name: 'AdminUnitConfig'
	has_many :children, class_name: 'AdminUnitConfig', foreign_key: 'parent_id'
end
