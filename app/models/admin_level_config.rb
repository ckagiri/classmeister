class AdminLevelConfig < ActiveRecord::Base
	has_many :admin_unit_configs, foreign_key: :admin_level_id
	belongs_to :parent, class_name: 'AdminLevelConfig'
	has_many :children, class_name: 'AdminLevelConfig', foreign_key: 'parent_id'
end
