class AdminLevelConfigSerializer < ActiveModel::Serializer
  attributes :id, :name, :description, :parent_id, :status
  root 'admin_level'
end
