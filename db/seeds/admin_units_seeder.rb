require 'roo'

class AdminUnitsSeeder
	@@admin_levels
	@@counties = []
	@@constituencies = []

	def self.seed 
		data = parse_file
		load_admin_levels
  	build_counties(data)
  	build_constituencies(data)
  	build_wards(data)
	end

	def self.parse_file
		xl = Roo::Spreadsheet.open('./db/seeds/source/IEBC Electoral Boundaries.xlsx')
		xl.default_sheet = xl.sheets[1]
		header = xl.row(1).map do |x| 
  		x.delete('.').downcase.gsub(/\s+/, "_").to_sym 
		end
		data = []
		(2..xl.last_row).each do |i| 
  		row = Hash[[header, xl.row(i)].transpose]
  		data.push(row)
		end  	
		data
	end

	def self.load_admin_levels
		@@admin_levels = AdminLevelConfig.all
	end

	def self.build_counties(data)
		counties = get_counties(data)
		admin_level = @@admin_levels.find{ |n| n[:name] == 'County' }
		counties.each do |line|
  		au = AdminUnitConfig.create!(name: line[:county_name].capitalize, admin_level: admin_level)
  		@@counties << au
		end
 	end  

 	def self.get_counties(data) 
		data.group_by{ |n| n[:county_code] }.values.map { |n| n.first }
  end

  def self.build_constituencies(data)
  	consts = get_constituencies(data)
  	admin_level = @@admin_levels.find{ |n| n[:name] == 'Constituency' }
  	consts.each	do |line|
  		county = @@counties.find{ |n| n[:name] == line[:county_name].capitalize }
  		au = AdminUnitConfig.create!(name: line[:const_name].capitalize, parent: county, admin_level: admin_level)
  		@@constituencies << au
  	end
  end

  def self.get_constituencies(data)
  	data.group_by{ |n| n[:const_code] }.values.map { |n| n.first }
  end

  def self.build_wards(data)
  	wards = get_wards(data)
  	admin_level = @@admin_levels.find{ |n| n[:name]== 'Ward' }
  	wards.each do |line|
  		const = @@constituencies.find{ |n| n[:name] == line[:const_name].capitalize }
  		au = AdminUnitConfig.create!(name: line[:county_assembly_ward_name].capitalize, parent: const, admin_level: admin_level)
  	end  	
  end

  def self.get_wards(data)
  	data.group_by{ |n| n[:caw_code] }.values.map { |n| n.first }
  end
end