class AdminLevelsSeeder
	def self.seed
		county = AdminLevelConfig.create!(name: 'County')
		consistuency = AdminLevelConfig.create!(name: 'Constituency', parent: county)
		AdminLevelConfig.create!(name: 'Ward', parent: consistuency)
		AdminLevelConfig.create!(name: 'Zone')
		AdminLevelConfig.create!(name: 'Division')
		AdminLevelConfig.create!(name: 'Sub County', parent: county)
	end
end