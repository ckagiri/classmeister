require './db/seeds/admin_levels_seeder'
require './db/seeds/admin_units_seeder'

AdminLevelsSeeder.seed
AdminUnitsSeeder.seed