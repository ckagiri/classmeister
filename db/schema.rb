# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160422211030) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "academic_level_configs", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.string   "status"
    t.integer  "institution_level_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  add_index "academic_level_configs", ["institution_level_id"], name: "index_academic_level_configs_on_institution_level_id", using: :btree

  create_table "academic_unit_tutors", force: :cascade do |t|
    t.integer "academic_unit_id"
    t.integer "tutor_id"
    t.string  "year"
  end

  add_index "academic_unit_tutors", ["academic_unit_id"], name: "index_academic_unit_tutors_on_academic_unit_id", using: :btree
  add_index "academic_unit_tutors", ["tutor_id"], name: "index_academic_unit_tutors_on_tutor_id", using: :btree

  create_table "academic_units", force: :cascade do |t|
    t.integer  "academic_level_id"
    t.string   "name"
    t.integer  "institution_id"
    t.integer  "parent_id"
    t.integer  "boys"
    t.integer  "girls"
    t.string   "status"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "academic_units", ["academic_level_id"], name: "index_academic_units_on_academic_level_id", using: :btree
  add_index "academic_units", ["institution_id"], name: "index_academic_units_on_institution_id", using: :btree

  create_table "admin_level_configs", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.integer  "parent_id"
    t.string   "status"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "admin_level_configs", ["name"], name: "index_admin_level_configs_on_name", unique: true, using: :btree

  create_table "admin_unit_configs", force: :cascade do |t|
    t.string   "name"
    t.integer  "parent_id"
    t.string   "description",    default: ""
    t.string   "status"
    t.integer  "admin_level_id"
    t.string   "code"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  add_index "admin_unit_configs", ["admin_level_id"], name: "index_admin_unit_configs_on_admin_level_id", using: :btree
  add_index "admin_unit_configs", ["parent_id"], name: "index_admin_unit_configs_on_parent_id", using: :btree

  create_table "institution_level_configs", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.string   "status"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "institution_level_configs", ["name"], name: "index_institution_level_configs_on_name", unique: true, using: :btree

  create_table "institutions", force: :cascade do |t|
    t.string   "code"
    t.string   "name"
    t.integer  "base_admin_unit_id"
    t.string   "private_public_status"
    t.string   "gender_admitted"
    t.string   "day_boarding_status"
    t.string   "integrated_or_special_status"
    t.integer  "boys_toilets"
    t.integer  "girls_toilets"
    t.boolean  "pupil_toilet_shared"
    t.date     "date_started"
    t.string   "lat"
    t.string   "long"
    t.string   "status"
    t.integer  "institution_level_id"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  add_index "institutions", ["base_admin_unit_id"], name: "index_institutions_on_base_admin_unit_id", using: :btree
  add_index "institutions", ["institution_level_id"], name: "index_institutions_on_institution_level_id", using: :btree

  create_table "tutors", force: :cascade do |t|
    t.string   "name"
    t.string   "reg_no"
    t.string   "id_no"
    t.integer  "institution_id"
    t.integer  "admin_unit_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "tutors", ["admin_unit_id"], name: "index_tutors_on_admin_unit_id", using: :btree
  add_index "tutors", ["institution_id"], name: "index_tutors_on_institution_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
