class CreateAcademicLevelConfigs < ActiveRecord::Migration
  def change
    create_table :academic_level_configs do |t|
      t.string :name
      t.string :description
      t.string :status
      t.integer :institution_level_id

      t.timestamps null: false
    end
    add_index :academic_level_configs, :institution_level_id
  end
end
