class CreateInstitutions < ActiveRecord::Migration
  def change
    create_table :institutions do |t|
      t.string :code
      t.string :name
      t.integer :base_admin_unit_id
      t.string :private_public_status
      t.string :gender_admitted
      t.string :day_boarding_status
      t.string :integrated_or_special_status
      t.integer :boys_toilets
      t.integer :girls_toilets
      t.boolean :pupil_toilet_shared
      t.date :date_started
      t.string :lat
      t.string :long
      t.string :status
      t.integer :institution_level_id

      t.timestamps null: false
    end
    add_index :institutions, :base_admin_unit_id
    add_index :institutions, :institution_level_id
  end
end
