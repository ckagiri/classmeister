class CreateAcademicUnitTutor < ActiveRecord::Migration
  def change
    create_table :academic_unit_tutors do |t|
      t.integer :academic_unit_id
      t.integer :tutor_id
      t.string :year
    end
    add_index :academic_unit_tutors, :academic_unit_id
    add_index :academic_unit_tutors, :tutor_id
  end
end
