class CreateTutors < ActiveRecord::Migration
  def change
    create_table :tutors do |t|
      t.string :name
      t.string :reg_no
      t.string :id_no
      t.integer :institution_id
      t.integer :admin_unit_id

      t.timestamps null: false
    end
    add_index :tutors, :institution_id
    add_index :tutors, :admin_unit_id
  end
end
