class CreateInstitutionLevelConfigs < ActiveRecord::Migration
  def change
    create_table :institution_level_configs do |t|
      t.string :name
      t.string :description
      t.string :status

      t.timestamps null: false
    end
    add_index :institution_level_configs, :name, unique: true
  end
end
