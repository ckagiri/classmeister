class CreateAcademicUnits < ActiveRecord::Migration
  def change
    create_table :academic_units do |t|
      t.integer :academic_level_id
      t.string :name
      t.integer :institution_id
      t.integer :parent_id
      t.integer :boys
      t.integer :girls
      t.string :status

      t.timestamps null: false
    end
    add_index :academic_units, :academic_level_id
    add_index :academic_units, :institution_id
  end
end
