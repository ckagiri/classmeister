class CreateAdminUnitConfigs < ActiveRecord::Migration
  def change
    create_table :admin_unit_configs do |t|
      t.string :name
      t.integer :parent_id
      t.string :description, default: ""
      t.string :status
      t.integer :admin_level_id
      t.string :code

      t.timestamps null: false
    end
    add_index :admin_unit_configs, :parent_id
    add_index :admin_unit_configs, :admin_level_id
  end
end
