class CreateAdminLevelConfigs < ActiveRecord::Migration
  def change
    create_table :admin_level_configs do |t|
      t.string :name
      t.string :description
      t.integer :parent_id
      t.string :status

      t.timestamps null: false
    end
    add_index :admin_level_configs, :name, unique: true
  end
end
